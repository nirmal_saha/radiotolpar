package com.powergroupbd.radiotolpar.jsonparser;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TrackStatisticsParser {

	DefaultHttpClient client;
	JSONObject json;
	String Url;

	public TrackStatisticsParser(String Url) {

		this.Url = Url;
		client = new DefaultHttpClient();

	}

	public String parseInitiator() throws ClientProtocolException, IOException,
			JSONException {

		StringBuilder url = new StringBuilder(Url);

		HttpGet get = new HttpGet(url.toString());
		HttpResponse r = client.execute(get);
		int statusrecev = r.getStatusLine().getStatusCode();
		if (statusrecev == 200) {

			HttpEntity e = r.getEntity();
			String data = EntityUtils.toString(e);

			JSONObject firstobj = new JSONObject(data);
			JSONArray firstarr = firstobj.getJSONArray("streams");
			JSONObject secondobj = firstarr.getJSONObject(0);
			String SongTitle = secondobj.getString("songtitle");
			return SongTitle;
		}
		return "Title - Artist";
	}

}
