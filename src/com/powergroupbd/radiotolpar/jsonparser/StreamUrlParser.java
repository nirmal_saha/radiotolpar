package com.powergroupbd.radiotolpar.jsonparser;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StreamUrlParser {

	String updatedate;
	DefaultHttpClient client;
	JSONObject json;
	String Url;

	public StreamUrlParser(String Url) {

		this.Url = Url;
		client = new DefaultHttpClient();
	}

	public String parseInitiator() throws ClientProtocolException, IOException,
			JSONException {
		String streamUrl="";
		StringBuilder url = new StringBuilder(Url);
		HttpGet get = new HttpGet(url.toString());
		HttpResponse r = client.execute(get);
		int statusrecev = r.getStatusLine().getStatusCode();
		if (statusrecev == 200) {
			HttpEntity entity = r.getEntity();
			String data = EntityUtils.toString(entity);
			
			JSONObject o = new JSONObject(data);
			JSONArray array= o.getJSONArray("data");
			JSONObject obj= new JSONObject();
			for(int i=0; i< array.length();i++){
				
				obj= array.getJSONObject(i);
				streamUrl= obj.getString("link");
				
				
			}
			
			
			return streamUrl;
			
			

		} else {

			return "";
		}

	}
}
