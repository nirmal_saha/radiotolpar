package com.powergroupbd.radiotolpar.jsonparser;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.powergroupbd.radiotolpar.utility.schedulegettersetter;

public class Scheduleparser {

	String updatedate;
	DefaultHttpClient client;
	JSONObject json;
	String Url;
	ArrayList<schedulegettersetter> arrschedule;

	public Scheduleparser(String Url) {

		this.Url = Url;
		client = new DefaultHttpClient();
		arrschedule = new ArrayList<schedulegettersetter>();
	}

	public ArrayList<schedulegettersetter> parseInitiator() throws ClientProtocolException, IOException,
			JSONException {
		String day = "";
		String programe = "";
		String time = "";
		String duration = "";
		StringBuilder url = new StringBuilder(Url);
		HttpGet get = new HttpGet(url.toString());
		HttpResponse r = client.execute(get);
		int statusrecev = r.getStatusLine().getStatusCode();
		if (statusrecev == 200) {
			HttpEntity entity = r.getEntity();
			String data = EntityUtils.toString(entity);

			JSONObject o = new JSONObject(data);
			JSONArray array = o.getJSONArray("data");
			JSONObject obj = new JSONObject();
			for (int i = 0; i < array.length(); i++) {

				obj = array.getJSONObject(i);
				day = obj.getString("Day");
				programe = obj.getString("ProgramName");
				time = obj.getString("Time");

				duration = obj.getString("Duration");
				
				arrschedule.add(new schedulegettersetter(day, programe,time, duration
						));
			}

			return arrschedule;

		} else {

			return null;
		}

	}
}
