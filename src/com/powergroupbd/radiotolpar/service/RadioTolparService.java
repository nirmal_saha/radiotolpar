package com.powergroupbd.radiotolpar.service;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnErrorListener;
import io.vov.vitamio.MediaPlayer.OnPreparedListener;

import java.io.IOException;

import com.powergroupbd.radiotolpar.MainActivity;
import com.powergroupbd.radiotolpar.R;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;

import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class RadioTolparService extends Service implements OnPreparedListener,
		OnErrorListener {

	MediaPlayer player;
	String url;

	PhoneStateListener psl;
	TelephonyManager tManager;

	public static final String ACTION_RESUME = "com.powergroupbd.radiotolpar.service.action.resume";
	public static final String ACTION_PAUSE = "com.powergroupbd.radiotolpar.service.action.pause";
	public static final String ACTION_SETUP_AND_PLAY = "com.powergroupbd.radiotolpar.service.action.setupandplay";

	public static final String RECIEVER_ACTION_PLAYING = "com.powergroupbd.radiotolpar.playing";
	public static final String RECIEVER_ACTION_STOPPED = "com.powergroupbd.radiotolpar.stopped";
	public static final String RECIEVER_ACTION_PREPARING = "com.powergroupbd.radiotolpar.preparing";

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		player = new MediaPlayer(this);
		url = "http://neon.wavestreamer.com:6129";
		handlePhoneCall();
		player.setOnPreparedListener(this);
		player.setOnErrorListener(this);

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		if (intent.getAction().contentEquals(ACTION_SETUP_AND_PLAY))
			playerSetUpAndPlay();
		else if (intent.getAction().contentEquals(ACTION_PAUSE))
			playerPause();
		else if (intent.getAction().contentEquals(ACTION_RESUME))
			playerResume();
		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		playerStopANdRealease();
	}

	private void handlePhoneCall() {
		psl = new PhoneStateListener() {

			@Override
			public void onCallStateChanged(int state, String incomingNumber) {

				Log.e("PhonState", "Changed");
				if (state == TelephonyManager.CALL_STATE_RINGING) {
					// Incoming call: Pause music
					Log.e("PhonState", "Ringing");
					if (player != null) {
						if (player.isPlaying()) {

							player.pause();

						}
					}
				} else if (state == TelephonyManager.CALL_STATE_IDLE) {
					// Not in call: Play music
					Log.e("PhonState", "Idle");
					if (player != null) {
						if (!player.isPlaying()) {
							player.start();

						}
					}

				} else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
					// A call is dialing, active or on hold
					Log.e("PhonState", "Dialing");
					if (player != null) {
						if (player.isPlaying()) {

							player.pause();

						}
					}
				}

				// TODO Auto-generated method stub
				super.onCallStateChanged(state, incomingNumber);
			}

		};
		tManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if (tManager != null) {
			tManager.listen(psl, PhoneStateListener.LISTEN_CALL_STATE);
		}

	}

	private void playerSetUpAndPlay() {
		try {
			if (player != null) {
				if (!player.isPlaying()) {
					player.setDataSource(url);
					player.prepareAsync();
					// player.start();
					broadCast(RECIEVER_ACTION_PREPARING);

				}

			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void playerStopANdRealease() {
		if (player != null) {

			if (player.isPlaying()) {

				player.stop();

			}
			player.release();
			player = null;

			broadCast(RECIEVER_ACTION_STOPPED);

		}

	}

	private void playerResume() {
		if (player != null) {

			if (!player.isPlaying()) {

				player.start();
				broadCast(RECIEVER_ACTION_PLAYING);

			}

		}

	}

	private void playerPause() {
		if (player != null) {

			if (player.isPlaying()) {

				player.pause();

				broadCast(RECIEVER_ACTION_STOPPED);

			}
		}

	}

	private void broadCast(String recieverAction) {
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(recieverAction);
		sendBroadcast(broadcastIntent);

	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		player.start();
		if (player.isPlaying()) {

			pushServicetoForeground();

			broadCast(RECIEVER_ACTION_PLAYING);

		}

	}

	private void pushServicetoForeground() {
		final int notiId = 1234;
		Notification notice;
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setContentText("Playing").setAutoCancel(true);

		Intent notificationIntent = new Intent(this, MainActivity.class);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(contentIntent);
		notice = builder.build();
		startForeground(notiId, notice);

	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		return false;
	}

}
