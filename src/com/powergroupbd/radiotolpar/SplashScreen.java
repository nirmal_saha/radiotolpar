package com.powergroupbd.radiotolpar;

import com.powergroupbd.radiotolpar.utility.ConnectivityReceiver;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class SplashScreen extends Activity {

	boolean netconnected = false;
	ConnectivityReceiver connect;
	SharedPreferences preference;
	public static final String filename = "ozmoicon";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash);

		connect = new ConnectivityReceiver(this);
		preference = getSharedPreferences(filename, 0);
		new SplashBackground().execute();

	}

	@Override
	protected void onPause() {
		super.onPause();
		connect.unbind(SplashScreen.this);
		finish();
	}

	@Override
	protected void onResume() {
		Log.i("CalledSplash", "OnResume");
		connect.bind(SplashScreen.this);
		super.onResume();
	}

	@Override
	protected void onStop() {
		Log.i("CalledSplash", "OnStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i("CalledSplash", "OnDestroy");
		super.onDestroy();
	}

	class SplashBackground extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			if (connect.hasConnection()) {
				netconnected = true;

			}

			else {

				netconnected = false;

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Looper.prepare();
			Thread timer = new Thread() {

				public void run() {
					// Looper.prepare();
					try {
						sleep(3000);

					} catch (InterruptedException e) {

						e.printStackTrace();
					} finally {

						if (netconnected) {

							Intent go = new Intent(
									"com.powergroupbd.radiotolpar.MAINACTIVITY");
							startActivity(go);

						} else {

							runOnUiThread(new Runnable() {
								public void run() {
									AlertDialog.Builder verifyalert = new AlertDialog.Builder(
											SplashScreen.this);
									verifyalert.setCancelable(false);
									verifyalert.setTitle("Information");
									TextView myMsg = new TextView(
											SplashScreen.this);
									myMsg.setText("Please check your internet connection");
									myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
									myMsg.setTextColor(Color.WHITE);
									verifyalert.setView(myMsg);

									verifyalert
											.setPositiveButton(
													"Ok",
													new DialogInterface.OnClickListener() {

														public void onClick(
																DialogInterface dialog,
																int which) {

															finish();

														}
													});

									verifyalert.show();

								}
							});

						}

					}

				}

			};
			timer.start();

		}

	}
}
