package com.powergroupbd.radiotolpar.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import com.powergroupbd.radiotolpar.R;
import com.powergroupbd.radiotolpar.jsonparser.Scheduleparser;
import com.powergroupbd.radiotolpar.utility.PublicValue;
import com.powergroupbd.radiotolpar.utility.Scheduleadapter;
import com.powergroupbd.radiotolpar.utility.schedulegettersetter;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class Schedule extends Fragment {
	ImageView scheduleheaderimage;
	ListView view_shedule;
	ArrayAdapter<schedulegettersetter> scheduleadapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.schedule, container, false);

		scheduleheaderimage = (ImageView) v.findViewById(R.id.scheduleheader);
		view_shedule = (ListView) v.findViewById(R.id.webview_shedule);
		new ListLoader().execute();
		return v;
	}

	public class ListLoader extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try {

				Scheduleparser scheduleparser = new Scheduleparser(
						PublicValue.scheduleUrl);
				PublicValue.arrschedule = scheduleparser.parseInitiator();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			scheduleadapter = new Scheduleadapter(getActivity(), getModel());
			view_shedule.setAdapter(scheduleadapter);

		}

		private List<schedulegettersetter> getModel() {
			List<schedulegettersetter> list = new ArrayList<schedulegettersetter>();

			for (int i = 0; i < PublicValue.arrschedule.size(); i++) {

				list.add(get(PublicValue.arrschedule.get(i).getDay(),
						PublicValue.arrschedule.get(i).getProgramename(),
						PublicValue.arrschedule.get(i).getTime(),
						PublicValue.arrschedule.get(i).getDuration()));

			}

			return list;
		}

		private schedulegettersetter get(String day, String prog, String time,
				String duration) {
			return new schedulegettersetter(day, prog, time, duration);
		}

	}
}
