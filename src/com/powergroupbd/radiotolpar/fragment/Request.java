package com.powergroupbd.radiotolpar.fragment;

import com.powergroupbd.radiotolpar.MainActivity;
import com.powergroupbd.radiotolpar.R;
import com.powergroupbd.radiotolpar.utility.PublicValue;
import com.powergroupbd.radiotolpar.utility.Wvclient;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;
import android.widget.TextView;

public class Request extends Fragment {
	WebView view_request;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.request, container, false);

		view_request = (WebView) v.findViewById(R.id.webview_request);
		AlertDialog.Builder verifyalert = new AlertDialog.Builder(getActivity());
		verifyalert.setTitle("Information");
		TextView myMsg = new TextView(getActivity());
		myMsg.setText("Please allow us upto 30 minutes to process your requested songs."
				+ "\n"
				+ "During public holidays & Weekends it may take longer due to high volume of requests"
				+ "\n"
				+ "Please stay tuned till then.....Thank you for listening- Radio Tolpar");
		myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
		myMsg.setTextColor(Color.BLACK);
		verifyalert.setView(myMsg);

		verifyalert.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		verifyalert.show();
		LoadWebView();
		return v;
	}

	private void LoadWebView() {
		// TODO Auto-generated method stub
		view_request.setHorizontalScrollBarEnabled(false);
		view_request.setVerticalScrollBarEnabled(true);
		view_request.getSettings().setJavaScriptEnabled(true);
		view_request.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		view_request.getSettings().setPluginState(PluginState.ON);
		view_request.getSettings().setUseWideViewPort(true);
		view_request.requestFocus(View.FOCUS_DOWN);
		view_request.setWebViewClient(new Wvclient());
		view_request.loadUrl(PublicValue.requestUrl);

	}
	// @Override
	// public boolean onKeyDown(int keyCode, KeyEvent event) {
	// if (event.getAction() == KeyEvent.ACTION_DOWN) {
	// switch (keyCode) {
	// case KeyEvent.KEYCODE_BACK:
	// if (view_request.canGoBack() == true) {
	// view_request.goBack();
	// } else {
	// finish();
	// }
	// return true;
	// }
	//
	// }
	// return super.onKeyDown(keyCode, event);
	// }

}
