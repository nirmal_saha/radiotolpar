package com.powergroupbd.radiotolpar.fragment;

import com.powergroupbd.radiotolpar.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Aboutus extends Fragment {
	ImageView view_aboutus;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.about_us, container, false);
		view_aboutus = (ImageView) v.findViewById(R.id.imgview_aboutus);

		return v;
	}

}
