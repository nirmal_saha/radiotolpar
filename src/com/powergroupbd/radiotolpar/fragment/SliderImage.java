package com.powergroupbd.radiotolpar.fragment;

import com.powergroupbd.radiotolpar.R;
import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class SliderImage extends Fragment {

	ImageView homescreen;

	int imagesToShow[] = { R.drawable.five, R.drawable.six, R.drawable.two,
			R.drawable.three, R.drawable.four, R.drawable.one };

	int start = 0;
	MyCount count;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.slider_image, container, false);
		initializer(v);
		count.start();
		return v;
	}

	private void initializer(View v) {
		count = new MyCount(imagesToShow.length * 5000, 5000);
		homescreen = (ImageView) v.findViewById(R.id.imgview_home);

	}

	public class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {

			count.start();

		}

		@Override
		public void onTick(long millisUntilFinished) {
			if (start < imagesToShow.length) {
				 homescreen.setImageResource(imagesToShow[start]);
				start += 1;
			} else {

				start = 0;
				 homescreen.setImageResource(imagesToShow[start]);
			}

		}
	}
}
