package com.powergroupbd.radiotolpar;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Webshow extends Activity{
	WebView web;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webshow);
		String url = getIntent().getStringExtra("url");

		web = (WebView) findViewById(R.id.webView1);
	    //next line explained below
		
		web.getSettings().setLoadWithOverviewMode(true);
		web.getSettings().setUseWideViewPort(true);
		web.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		web.getSettings().setPluginState(PluginState.ON);
		web.getSettings().setDomStorageEnabled(true);
		web.setWebViewClient(new myWebClient());
		web.getSettings().setJavaScriptEnabled(true);
		web.loadUrl(url);
		
	}

	public class myWebClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub

			super.onPageStarted(view, url, favicon);

		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub

				
		
				view.loadUrl(url);
			
			return true;
		      
		       
		    
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
		

			super.onPageFinished(view, url);
			
		

		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && web.canGoBack()) {
			web.goBack();
			WebSettings webSettings = web.getSettings();
			webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		

			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
}


