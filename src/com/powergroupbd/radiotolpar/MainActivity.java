package com.powergroupbd.radiotolpar;

import io.vov.vitamio.LibsChecker;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.powergroupbd.radiotolpar.fragment.Aboutus;
import com.powergroupbd.radiotolpar.fragment.Request;
import com.powergroupbd.radiotolpar.fragment.Schedule;
import com.powergroupbd.radiotolpar.fragment.SliderImage;
import com.powergroupbd.radiotolpar.jsonparser.TrackStatisticsParser;
import com.powergroupbd.radiotolpar.service.RadioTolparService;
import com.powergroupbd.radiotolpar.utility.PublicValue;

public class MainActivity extends Activity implements OnClickListener,
		OnSeekBarChangeListener {

	AdView adView;
	AdRequest bannerRequest, fullScreenAdRequest;
	InterstitialAd fullScreenAdd;

	Button btn_home, btn_request, btn_shedule, btn_aboutus, facebook,
			playmusic, rate;

	TextView tvTitle, tvArtist, tvmaintitle;
	SeekBar skVolume;

	String title;
	TrackStatisticsParser tsp;
	Handler handler = new Handler();
	Runnable r;
	AudioManager audioManager;

	BroadcastReceiver playerStateReceiver;
	Timer timer;
	TimerTask doAsynchronousTask;

	Intent serviceIntent;

	boolean isMusicstarted = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// add1 = (AdView) findViewById(R.id.add1);
		// // AdRequest request1 = new AdRequest();
		// // add1.loadAd(request1);
		initializer();
		getFragmentManager().beginTransaction()
				.replace(R.id.relmain, new SliderImage()).commit();
		btn_home.setBackgroundResource(R.drawable.presshome);

		refreshStatistics();

		/* My Code */

		btn_home.setOnClickListener(this);
		btn_request.setOnClickListener(this);
		btn_shedule.setOnClickListener(this);
		btn_aboutus.setOnClickListener(this);
		skVolume.setOnSeekBarChangeListener(this);
		playmusic.setOnClickListener(this);
		facebook.setOnClickListener(this);
		rate.setOnClickListener(this);
		/* Ends */

		
	}

	private void enableAd() {
		// adding banner add
		adView = (AdView) findViewById(R.id.adView);
		bannerRequest = new AdRequest.Builder().build();
		adView.loadAd(bannerRequest);

//		// adding full screen add
		fullScreenAdd = new InterstitialAd(this);
		fullScreenAdd.setAdUnitId("ca-app-pub-4109029600706011/2600772884");
		fullScreenAdRequest = new AdRequest.Builder().build();
		fullScreenAdd.loadAd(fullScreenAdRequest);

		fullScreenAdd.setAdListener(new AdListener() {

			@Override
			public void onAdLoaded() {

				Log.i("FullScreenAdd", "Loaded successfully");
				fullScreenAdd.show();

			}

			@Override
			public void onAdFailedToLoad(int errorCode) {

				Log.i("FullScreenAdd", "failed to Load");
			}
		});

	}

	private void initializer() {
		serviceIntent = new Intent(MainActivity.this, RadioTolparService.class);

		tsp = new TrackStatisticsParser(PublicValue.trackStatistics);

		tvTitle = (TextView) findViewById(R.id.tv_track);
		tvArtist = (TextView) findViewById(R.id.tv_singer);
		skVolume = (SeekBar) findViewById(R.id.seekbar_sound);
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		skVolume.setMax(audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		skVolume.setProgress(audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC));

		btn_home = (Button) findViewById(R.id.btn_home);
		btn_request = (Button) findViewById(R.id.btn_request);
		btn_shedule = (Button) findViewById(R.id.btn_shedule);
		btn_aboutus = (Button) findViewById(R.id.btn_aboutus);

		facebook = (Button) findViewById(R.id.btn_facebook);
		rate = (Button) findViewById(R.id.btn_rateap);

		playmusic = (Button) findViewById(R.id.newplay);

		tvmaintitle = (TextView) findViewById(R.id.tv_title);

		playerStateReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub

				if (intent.getAction() == RadioTolparService.RECIEVER_ACTION_PREPARING) {

					Log.i("receiver called", "buffering");
					playmusic.setEnabled(false);
					isMusicstarted = false;
					playmusic.setBackgroundResource(R.drawable.pause);

				} else if (intent.getAction() == RadioTolparService.RECIEVER_ACTION_PLAYING) {
					Log.i("receiver called", "playing");
					isMusicstarted = true;
					playmusic.setEnabled(true);
					playmusic.setBackgroundResource(R.drawable.pause);
					enableAd();

				} else if (intent.getAction() == RadioTolparService.RECIEVER_ACTION_STOPPED) {
					Log.i("receiver called", "Stopped");
					isMusicstarted = false;
					playmusic.setEnabled(true);
					playmusic.setBackgroundResource(R.drawable.play);

				}

			}
		};

		if (!LibsChecker.checkVitamioLibs(this))
			return;

		startRadioPlayService(RadioTolparService.ACTION_SETUP_AND_PLAY);
		

	}

	private void startRadioPlayService(String action) {

		serviceIntent.setAction(action);
		startService(serviceIntent);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		IntentFilter filter = new IntentFilter();
		filter.addAction(RadioTolparService.RECIEVER_ACTION_PLAYING);
		filter.addAction(RadioTolparService.RECIEVER_ACTION_PREPARING);
		filter.addAction(RadioTolparService.RECIEVER_ACTION_STOPPED);

		registerReceiver(playerStateReceiver, filter);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (!isMusicstarted) {

			stopService(serviceIntent);
		}
		unregisterReceiver(playerStateReceiver);
	}

	public void refreshStatistics() {
		handler = new Handler();
		timer = new Timer();
		doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						try {
							new StatisticsLoader().execute();
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
					}
				});
			}
		};
		timer.schedule(doAsynchronousTask, 0, 30000);

	}

	public void onClick(View v) {

		switch (v.getId()) {
		/* My Code */

		case R.id.newplay:

			// buttonToggle();
			// sif(v.get)
			if (isMusicstarted)
				startRadioPlayService(RadioTolparService.ACTION_PAUSE);
			else
				startRadioPlayService(RadioTolparService.ACTION_RESUME);
			break;

		case R.id.btn_home:

			btn_home.setBackgroundResource(R.drawable.presshome);
			btn_request.setBackgroundResource(R.drawable.request);
			btn_shedule.setBackgroundResource(R.drawable.schedule);
			btn_aboutus.setBackgroundResource(R.drawable.aboutus);

			getFragmentManager().beginTransaction()
					.replace(R.id.relmain, new SliderImage()).commit();
			break;

		case R.id.btn_request:

			btn_home.setBackgroundResource(R.drawable.home);
			btn_request.setBackgroundResource(R.drawable.pressrequest);
			btn_shedule.setBackgroundResource(R.drawable.schedule);
			btn_aboutus.setBackgroundResource(R.drawable.aboutus);

			getFragmentManager().beginTransaction()
					.replace(R.id.relmain, new Request()).commit();

			break;

		case R.id.btn_shedule:

			btn_home.setBackgroundResource(R.drawable.home);
			btn_request.setBackgroundResource(R.drawable.request);
			btn_shedule.setBackgroundResource(R.drawable.pressschedule);
			btn_aboutus.setBackgroundResource(R.drawable.aboutus);

			getFragmentManager().beginTransaction()
					.replace(R.id.relmain, new Schedule()).commit();
			break;

		case R.id.btn_aboutus:
			btn_home.setBackgroundResource(R.drawable.home);
			btn_request.setBackgroundResource(R.drawable.request);
			btn_shedule.setBackgroundResource(R.drawable.schedule);
			btn_aboutus.setBackgroundResource(R.drawable.pressabout);

			getFragmentManager().beginTransaction()
					.replace(R.id.relmain, new Aboutus()).commit();
			break;
		case R.id.btn_facebook:

			Intent i = new Intent(MainActivity.this, Webshow.class);
			i.putExtra("url", "https://www.facebook.com/RadioTolpar?fref=ts");
			startActivity(i);
			break;
		case R.id.btn_rateap:
			Intent intent = new Intent(Intent.ACTION_VIEW);

			intent.setData(Uri
					.parse("market://details?id=com.powergroupbd.radiotolpar"));

			startActivity(intent);
			break;
		default:
			break;
		}

	}

	/* Ends */

	// private void buttonToggle() {
	// if (isMusicstarted) {
	//
	// serviceIntent.putExtra("startMusic", "pause");
	// startService(serviceIntent);
	//
	// playmusic.setBackgroundResource(R.drawable.play);
	// isMusicstarted = false;
	// } else {
	//
	// // if (!PublicValue.streamUrl.contentEquals("")) {
	// serviceIntent.putExtra("startMusic", "resume");
	// startService(serviceIntent);
	// isMusicstarted = true;
	// playmusic.setBackgroundResource(R.drawable.pause);
	// // }
	// }
	//
	// }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i("Called", "OnDestroy");

		// stopService(serviceIntent);

		timer.cancel();
		doAsynchronousTask.cancel();
		handler.removeCallbacks(r);

	}

	public class StatisticsLoader extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			try {
				title = tsp.parseInitiator();
			} catch (ClientProtocolException e) {
				title = "Title-Artist";
				e.printStackTrace();
			} catch (IOException e) {
				title = "Title-Artist";
				e.printStackTrace();
			} catch (JSONException e) {
				title = "Title-Artist";
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			try {
				Log.i("Title", title);
				tvTitle.setVisibility(View.VISIBLE);
				tvmaintitle.setVisibility(View.VISIBLE);
				String[] subTitles = title.split("-");
				tvTitle.setText(subTitles[1]);
				tvTitle.setSelected(true);
				tvArtist.setText(subTitles[0]);
				tvArtist.setSelected(true);
			} catch (Exception e) {
				tvTitle.setVisibility(View.INVISIBLE);
				tvmaintitle.setVisibility(View.INVISIBLE);
				tvArtist.setText(title);
			}

		}

	}

	public void onProgressChanged(SeekBar arg0, int vol, boolean arg2) {
		// TODO Auto-generated method stub
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, vol, 0);
	}

	public void onStartTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub

	}

	public void onStopTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onBackPressed() {
	  
		
		new AlertDialog.Builder(this)
	    .setTitle("Like Our Facebook page")
	  .setPositiveButton("Like It", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	        	Intent i = new Intent(MainActivity.this, Webshow.class);
				i.putExtra("url", "https://www.facebook.com/RadioTolpar?fref=ts");
				startActivity(i);
	        }
	     })
	    .setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	           finish();
	        }
	     })
	    .setIcon(R.drawable.ic_launcher)
	     .show();
		
		
	  }
}
