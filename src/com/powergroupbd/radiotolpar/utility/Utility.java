package com.powergroupbd.radiotolpar.utility;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.util.Log;

public class Utility {

	public static boolean isServiceRunning(Context c, String ServiceName) {
		ActivityManager manager = (ActivityManager) c
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			Log.i("Class name of service", service.service.getClassName());
			if (ServiceName.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

}
