package com.powergroupbd.radiotolpar.utility;

public class schedulegettersetter {

	String day, time, duration, programename;

	public schedulegettersetter(String day, String programename,String time, String duration) {
		
		this.day=day;
		this.programename=programename;
		this.time=time;
		this.duration=duration;
		
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getProgramename() {
		return programename;
	}

	public void setProgramename(String programename) {
		this.programename = programename;
	}

}
