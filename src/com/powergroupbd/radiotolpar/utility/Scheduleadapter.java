package com.powergroupbd.radiotolpar.utility;

import java.util.List;

import com.powergroupbd.radiotolpar.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Scheduleadapter extends ArrayAdapter<schedulegettersetter> {
	private final List<schedulegettersetter> list;
	private final Activity context;
	View view = null;

	public Scheduleadapter(Activity context, List<schedulegettersetter> list) {
		super(context, R.layout.listrow, list);
		this.context = context;
		this.list = list;
	}

	static class ViewHolder {

		protected TextView day, programe, time, duration;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			view = inflator.inflate(R.layout.listrow, null);

			final ViewHolder viewHolder = new ViewHolder();

			viewHolder.day = (TextView) view.findViewById(R.id.txtday);
			viewHolder.day.setTextColor(Color.BLACK);

			viewHolder.programe = (TextView) view.findViewById(R.id.txtprog);
			viewHolder.programe.setTextColor(Color.BLACK);
			viewHolder.time = (TextView) view.findViewById(R.id.txtitme);
			viewHolder.time.setTextColor(Color.BLACK);
			viewHolder.duration = (TextView) view.findViewById(R.id.txtduration);
			viewHolder.duration.setTextColor(Color.BLACK);
			view.setTag(viewHolder);
			viewHolder.day.setTag(list.get(position));
			viewHolder.programe.setTag(list.get(position));
			viewHolder.time.setTag(list.get(position));
			viewHolder.duration.setTag(list.get(position));
			

		} else {
			view = convertView;
			((ViewHolder) view.getTag()).day.setTag(list.get(position));

			((ViewHolder) view.getTag()).programe.setTag(list.get(position));
			((ViewHolder) view.getTag()).time.setTag(list.get(position));
			((ViewHolder) view.getTag()).duration.setTag(list.get(position));
		}

		ViewHolder holder = (ViewHolder) view.getTag();
		holder.day.setText(list.get(position).getDay());
				holder.programe.setText(list.get(position).getProgramename());
				holder.time.setText(list.get(position).getTime());
				holder.duration.setText(list.get(position).getDuration());
				

	

		return view;
	}
}
